package Servlets;

import Model.Cutter;
import Model.DNAFragment;
import Model.Enzyme;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Processes data submitted by a user
 *
 * @author David
 */
public class SubmitDataServlet extends HttpServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher view = request.getRequestDispatcher("home.jsp");
        view.forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String[] enzymes = request.getParameterValues("enzymes");
        String sequence = request.getParameter("Sequence").toUpperCase().replaceAll("\\s|\\r\\n|\\n|\\r", "");
        HttpSession session = request.getSession();
        DNAFragment inSeq = new DNAFragment(sequence, 0, sequence.length(), 0);
        ArrayList<Enzyme> enzymeList = Enzyme.getEnzymes();
        ArrayList<Enzyme> enzUse = new ArrayList<>();
        if (enzymes != null) {
            for (String enz : enzymes) {
                enzymeList.stream().filter((en) -> (en.getName() == null ? enz == null : en.getName().equals(enz))).forEachOrdered((en) -> {
                    enzUse.add(en);
                });
            }
        }
        Cutter cut = new Cutter();
        ArrayList<DNAFragment> frags;
        if (request.getParameter("reverse").equals("true")) {
            frags = cut.cutSeq(inSeq, enzUse, true);
        } else {
            frags = cut.cutSeq(inSeq, enzUse, false);
        }

        request.setAttribute("frag", frags);
        RequestDispatcher view = request.getRequestDispatcher("home.jsp");
        view.forward(request, response);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
