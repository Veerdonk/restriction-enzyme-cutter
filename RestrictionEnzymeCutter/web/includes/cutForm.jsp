<%-- 
    Document   : cutForm
    Created on : Jan 25, 2017, 7:38:53 AM
    Author     : David van de Veerdonk
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<h1 class="headOfpage">Restriction enzyme cutter</h1>        
<div class="pure-u-1 pure-u-md-2-3">
    <p>You can enter the DNA sequence into the field below. 
        A selection can be made from 15 different restriction enzymes. 
        It is possible to select just one or multiple. </p>
    <p>Once a selection has been made
        choose whether the DNA stand is forward or reverse and hit submit. You will then
        be taken to a page containing information about each cut fragment.</p>
</div>

<form class="pure-form" name="inputForm" action="cut.do" method="POST">
    <div class="content-subhead">
        <label for="Sequence" class="pure-input-1-3">Your DNA sequence</label>
    </div><br/> 
    <p class="seqError errorMessage">Please enter a valid DNA sequence (only actg)</p> 
    <textarea class="pure-input-2-3" rows="5" name="Sequence" placeholder="Your sequence here"></textarea>

    <div class="content-subhead">
        <label for="enzymes" class="pure-checkbox">Choose which enzymes you want to use</label>
    </div><br/>
    <p class="boxError errorMessage">Please choose one or more restriction enzymes</p>
    <input type="checkbox" name="enzymes" value="AatII"> AatII
    <input type="checkbox" name="enzymes" value="Acc65I"> Acc65I
    <input type="checkbox" name="enzymes" value="AclI"> AccI
    <input type="checkbox" name="enzymes" value="MfeI"> MfeI
    <input type="checkbox" name="enzymes" value="MluI"> MluI
    <input type="checkbox" name="enzymes" value="MscI"> MscI
    <input type="checkbox" name="enzymes" value="BamHI"> BamHI
    <input type="checkbox" name="enzymes" value="AcII"> AcII <br/>
    <input type="checkbox" name="enzymes" value="HindIII"> HindIII
    <input type="checkbox" name="enzymes" value="SspI"> SspI
    <input type="checkbox" name="enzymes" value="MIuCI"> MIuCI
    <input type="checkbox" name="enzymes" value="Pcil"> Pcil
    <input type="checkbox" name="enzymes" value="Agel"> Agel
    <input type="checkbox" name="enzymes" value="MIuI"> MIuI
    <input type="checkbox" name="enzymes" value="HpyCH4IV"> HpyCH4IV 


    <div class="content-subhead">
        <label for="reverse">Is this sequence 5' -> 3' or reverse?</label>
    </div><br/>
    <input type="radio" name="reverse" checked="checked" value="false"> 5' -> 3'<br/>
    <input type="radio" name="reverse" value="true"> Reverse<br/>


    <button id="checkDNA" type="submit" class="pure-button pure-input-1-5 pure-button-primary">Submit</button>

</fieldset>
</form>
</body>
</html>
