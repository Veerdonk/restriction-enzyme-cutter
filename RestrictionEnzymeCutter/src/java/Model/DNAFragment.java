package Model;

/**
 * Class that describes a DNAFragment.
 * every fragment needs a sequence and ID, start and stop positions.
 * @author David
 */
public class DNAFragment {
    private final String seq;
    private final int seqid;
    private final int start;
    private final int stop;

    /**
     * Constructor for dna fragments
     * @param seq
     * @param start
     * @param stop
     * @param seqid
     */
    public DNAFragment(String seq, int start, int stop, int seqid) {
        this.seq = seq;
        this.start = start;
        this.stop = stop;
        this.seqid = seqid;
    }

    @Override
    public String toString() {
        return "DNAFragment{" + "seq=" + seq + ", start=" + start + ", stop=" + stop + '}';
    }

    /**
     * Returns the DNA sequence
     * @return
     */
    public String getSeq() {
        return seq;
    }

    /**
     * Returns the ID
     * @return
     */
    public int getID() {
        return seqid;
    }

    /**
     * Returns the start position
     * @return
     */
    public int getStart() {
        return start;
    }

    /**
     * resturn the stop position
     * @return
     */
    public int getStop() {
        return stop;
    }

    /**
     * Return the GC% of this fragment
     * Formula= (G+C)/(G+C+A+T)
     * @return
     */
    public float getGC(){
        long g = this.seq.chars().filter(ch -> ch == 'G').count();
        long c = this.seq.chars().filter(ch -> ch == 'C').count();
        float gc = (float)(g + c) / this.seq.length() * 100;
        gc = (float) (Math.round(gc * 100d) / 100d);
        return gc;
    }

    /**
     * Returns the molecular weight of this sequence rounded down to 2 decimal
     * places
     * @return
     */
    public double getMW(){
        double g = this.seq.chars().filter(ch -> ch == 'G').count() * 507.2;
        double c = this.seq.chars().filter(ch -> ch == 'C').count() * 467.2;
        double a = this.seq.chars().filter(ch -> ch == 'A').count() * 491.2;
        double t = this.seq.chars().filter(ch -> ch == 'T').count() * 482.2;
        double mw = Math.round((g + c + a + t) * 100d) / 100d;
        return mw;
    }

    /**
     * Returns the length of this fragment
     * @return
     */
    public int getLength(){
        return this.seq.length();
    }
}
