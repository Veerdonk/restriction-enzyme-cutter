<%-- 
    Document   : home
    Created on : Jan 13, 2017, 1:34:06 PM
    Author     : David van de Veerdonk
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
    <jsp:include page="includes/pageHead.jsp"/>
    <body>
        <jsp:include page="includes/sidebar.jsp"/>
                <c:choose>
                    <c:when test="${requestScope.frag != null}">
                        <jsp:include page="includes/results.jsp" />
                    </c:when>
                    <c:otherwise>
                        <jsp:include page="includes/cutForm.jsp"/>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </body>
</html>
