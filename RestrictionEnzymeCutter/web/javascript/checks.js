
window.onload = function () {
    //Checks wether a checkbox has been checked, if it has 
    //the sequence is checked for illegal characters
    function checkInput()
    {
        checked = $("input[type=checkbox]:checked").length;
        if (checked) {
            $('.boxError').hide();//hides the checkbox error
            var letters = /^[actgACTG]+$/;//regular expression for DNA
            if (document.forms["inputForm"]["Sequence"].value.replace(/(\s|\r\n|\n|\r)/gm,"").match(letters))
            {
                return true;
            } else
            {
                $('.seqError').show();//Shows error on DNA textarea
                return false;
            }
    }
    else{
        $('.boxError').show();
        return false;
    }}
    //Used to hide or show the sequences in results
    function showSeq() {
        $(".hiddenSeq").toggle(250);
    }
    $("#showSeq").click(showSeq);
    $("#checkDNA").click(checkInput);
};