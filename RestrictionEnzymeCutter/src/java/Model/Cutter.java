package Model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *Cutter.
 * Takes input sequences and cuts them based on restriction sites
 * @author David
 */
public class Cutter {

    /**
     * CutSeq.
     * takes a sequence and a list of enzymes then uses these enzymes to cut the
     * sequence into fragments. Returns an ArrayList of those fragments.
     * @param frag
     * @param enzymes
     * @param reverse
     * @return
     */
    public ArrayList<DNAFragment> cutSeq(DNAFragment frag, ArrayList<Enzyme> enzymes, boolean reverse){

        StringBuilder sb = new StringBuilder();
        HashMap cuts = new HashMap();
        enzymes.forEach((enzyme) -> {
            if(reverse == false){
                sb.append("(").append(enzyme.getRegEx()).append(")").append("|");
                cuts.put(enzyme.getRegEx(), enzyme.getLoc());
            }
            else{
                sb.append(enzyme.getReverseRegEx()).append("|");
                cuts.put(enzyme.getReverseRegEx(), enzyme.getReverseLoc());
            }
        });
        sb.setLength(sb.length()-1);
        Pattern pat = Pattern.compile(sb.toString());
        Matcher mat = pat.matcher(frag.getSeq());
        ArrayList<DNAFragment> frags = new ArrayList();
        int start = 0;
        DNAFragment nextfrg = frag;
        int groups = mat.groupCount();
        int seqid =0;
        while(mat.find()){
            int stop = mat.start()+ Integer.parseInt(cuts.get(mat.group()).toString());
            DNAFragment frg = new DNAFragment(frag.getSeq().substring(start, stop), start, stop, seqid);
            frags.add(frg);
            start = stop;
            seqid++;
        }
        DNAFragment last = new DNAFragment(frag.getSeq().substring(start, frag.getLength()), start, frag.getLength(), seqid);
        frags.add(last);
        return frags;
    }
}
