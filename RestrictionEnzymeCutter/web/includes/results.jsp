<%-- 
    Document   : results
    Created on : Jan 22, 2017, 9:25:51 PM
    Author     : David van de Veerdonk
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
    <jsp:include page="pageHead.jsp"/>
    <body>
        <h3>Fragments found in sequence:</h3>
        <button id="showSeq" class="pure-button shown-button" style="display:'';">Toggle sequences</button>
        <c:forEach var="frag" items="${requestScope.frag}">
            <div class="pure-g">
                <div class="pure-u-1-3 break-me">
                    <table class="pure-table pure-table-bordered">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>start</th>
                                <th>stop</th>
                                <th>length</th>
                                <th>GC%</th>
                                <th>Mol weigth</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>${frag.ID}</td>
                                <td>${frag.start}</td>
                                <td>${frag.stop}</td>
                                <td>${frag.length}</td>
                                <td>${frag.GC}</td>
                                <td>${frag.MW}</td>
                            </tr>
                        </tbody>
                    </table>
                            
                    <div class="hiddenSeq" style="display:none;">
                        ${frag.seq}
                    </div>
                    
                </div>
            </div>
        </c:forEach>
    </body>
</html>
