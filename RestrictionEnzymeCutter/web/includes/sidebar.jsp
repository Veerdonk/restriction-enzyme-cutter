<%-- 
    Document   : sidebar
    Created on : Jan 25, 2017, 10:31:05 PM
    Author     : David van de Veerdonk
--%>

<div id="layout" class="pure-g">
    <div class="sidebar pure-u-1 pure-u-md-1-4">
        <div class="header">
            <h1 class="brand-title">DNA Cutter</h1>
            <h2 class="brand-tagline">Cut DNA sequences with a selection of restriction enzymes</h2>
        </div>
    </div>
    <div class="content pure-u-1 pure-u-md-3-4">
