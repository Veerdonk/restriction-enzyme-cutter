# README #

A guide to use this restriction enzyme cutter and get it set up. 

### Basic usage ###

This web application can be used to simulate cutting strands of DNA with a variety of restriction. To get started you need a DNA sequence, this program only accepts sequences containing the DNA nucleotides A, C, T and G. You enter your DNA sequence into the text field and select one or more restriction enzymes for the simulation. A DNA strand can be forward or reverse, select the option that corresponds with our sequence. Once you click submit your sequence will be cut using the selected enzymes and you will be taken to a result page. Here you can review information about the fragments, their length, start position, stop position, GC content and molecular weight. The IDs are based on the order in which the fragments were cut. Clicking the show sequences button displays the fragments DNA sequence.

In short:

* Enter DNA sequence
* Select restriction enzymes
* Select forward or reverse
* View results

An example sequence you can use, when cut with AatII it should produce 3 fragments:
```
ATCCTCATAACCTTGGAGAGACGTCGCCGACAGAATGGGCACCTTGAAACTGCAGACGAGCATCTACG
ACAAAGCACATGACCACAAGGGACTATAGTAACATCAACTTCAGTGTTGAGACAGATGCGGCACAGCC
ATGCTGCTTTAGCTGTGTCAGCTTCTTTTGTAGCCATGTCAGACTTTTCCTGCTCGAGTAAGAGTGCC
GCTTGAGAATCCTTGAATTGCTCTTGCAATGTGATTGTTGTCTCCAGTAATGATTGCTTTTCCACGTC
CATACTGATTCCGACGTCAGAAAGCATCTCCTGAACTGCCTGGACTAATTCTGCAGCAGACACTCGGC
CATGTTGGAGTGCTTGAAGCTGCTGCTGGTGGTTCCCCTCAGAAGGTCTTGACTGAACTCTTCCAGTA
ACACCCTCACTCCTACTACTGTCCATCGTACAATAACCCTCAGGAAAAACAGCTGACGAATCCTCACC
AGAGATGGTGACACTTTTGAACGAGAAGACATGAGAAGATAGAAGAAGCTCAACCAATCCCAGTGATA
TTTCTACTTTGAATCTGTAGAGGGCTTGGCCAGCTGATGGTCTCACATTCTCTGAAACCCTACCATAC
CTCAACTTTTCTCCATTCTGCTGCCGCCAAGCTACTATTTCTCCAGCATAAAAGGGTCTTAATGGGTG
```
### How do I get set up? ###

This repository contains everything you need to get this application up and running. To get the web application running clean&build the project, find the . war file usually located in the dist folder and deploy it. This application was designed to run on a Tomcat 8.0.28 server.

This application uses two libraries:

* taglibs-standard-impl-1.2.5
* taglibs-standard-spec-1.2.5
* JQuery

It also uses Java8 functionality in the actual DNA cutter.

### Additional information ###

The layout of this application was designed using Purecss, grids and layout from [Pure](http://purecss.io/). For several input checks this application uses Javascript and JQuery.

### Who do I talk to? ###

If any problems arise please send me a message at d.van.de.veerdonk@st.hanze.nl and I'll be happy to assist you.