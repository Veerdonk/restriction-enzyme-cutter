package Model;

import java.util.ArrayList;

/**
 *This class describes the enzyme element.
 * Every enzyme needs a name, a regular expression
 * recognition site and the position it cuts
 * This class also contains a list of 15 enzymes currently supported
 * this can easily be expanded by adding extra to the list
 * @author David van de Veerdonk
 */
public class Enzyme {
    private final int loc;
    private final String name;
    private final String regEx;
    private static final ArrayList<Enzyme> enzymes;

    static {
        enzymes = new ArrayList<>();
        enzymes.add(new Enzyme(5, "AatII", "GACGTC"));
        enzymes.add(new Enzyme(1, "Acc65I", "GGTACC"));
        enzymes.add(new Enzyme(2, "AclI", "AACGTT"));
        enzymes.add(new Enzyme(1, "MfeI", "CAATTG"));
        enzymes.add(new Enzyme(1, "MluI", "ACGCGT"));
        enzymes.add(new Enzyme(3, "MscI", "TGGCCA"));
        enzymes.add(new Enzyme(1, "BamHI", "GGATCC"));
        enzymes.add(new Enzyme(2, "AcII", "AACGTT"));
        enzymes.add(new Enzyme(1, "HindIII", "AAGCTT"));
        enzymes.add(new Enzyme(3, "SspI", "AATATT"));
        enzymes.add(new Enzyme(0, "MIuCI", "AATT"));
        enzymes.add(new Enzyme(1, "Pcil", "ACATGT"));
        enzymes.add(new Enzyme(1, "Agel", "ACCGGT"));
        enzymes.add(new Enzyme(1, "MIuI", "ACGCGT"));
        enzymes.add(new Enzyme(1, "HpyCH4IV", "ACGT"));
    }

    /**
     *Constructor for enzymes.
     * @param loc
     * @param name
     * @param regEx
     */
    public Enzyme(int loc, String name, String regEx) {
        this.loc = loc;
        this.name = name;
        this.regEx = regEx;
    }

    /**
     * Retruns the cut location
     * @return
     */
    public int getLoc() {
        return loc;
    }

    /**
     * Resturns the enzyme name
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the recognition site
     * @return
     */
    public String getRegEx() {
        return regEx;
    }

    /**
     * Returns a reversed recognition site
     * @return
     */
    public String getReverseRegEx(){
        return new StringBuilder(regEx).reverse().toString();
    }

    /**
     * Returns the reverse cutting location
     * @return
     */
    public int getReverseLoc(){
        return regEx.length()-loc;
    }

    /**
     * Returns the list of enzymes contained in this class
     * @return
     */
    public static ArrayList<Enzyme> getEnzymes(){
        return enzymes;
    }
}
