<%-- 
    Document   : pageHead
    Created on : Jan 25, 2017, 7:37:12 AM
    Author     : David van de Veerdonk
--%>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>RestrictionEnzymeCutter</title>
    <link href="css/pure.css" rel="stylesheet">
    <link href="css/pureLayout.css" rel="stylesheet">
    <link href="css/pureGrids.css" rel="stylesheet">
    <script src="javascript/lib/jquery-3.1.1.min.js" type="text/javascript"></script>
    <script src="javascript/checks.js"></script>
</head>

